package com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.models;

/**
 * Created by sureshmaidaragi on 05/11/16.
 */

public class DataModel {
    public String regular_url;
    public String small_url;

    public String getRegular_url() {
        return regular_url;
    }

    public void setRegular_url(String regular_url) {
        this.regular_url = regular_url;
    }

    public String getSmall_url() {
        return small_url;
    }

    public void setSmall_url(String small_url) {
        this.small_url = small_url;
    }
}
