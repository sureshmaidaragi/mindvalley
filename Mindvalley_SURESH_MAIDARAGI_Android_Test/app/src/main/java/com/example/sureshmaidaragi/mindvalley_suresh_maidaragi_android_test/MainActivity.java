package com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;

import android.os.Message;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.adapter.DataAdapter;
import com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.models.DataModel;
import com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.threads.LoadDataThread;

import java.util.ArrayList;

public class MainActivity extends Activity {
    private ProgressBar loader_progressbar;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeLayout;
    private ProgressDialog mProgressDialog;
    private ArrayList<DataModel> datalist = new ArrayList<>();
    private DataAdapter mAdapter;

    private Handler mHandler = new Handler() {

        public void handleMessage(Message msg) {

            loader_progressbar.setVisibility(View.GONE);

            if (mProgressDialog != null) {
                mProgressDialog = null;
            }
            swipeLayout.setRefreshing(false);

            if (msg.what == LoadDataThread.LOADATA_SUCCESS) {
                datalist.clear();
                datalist = (ArrayList<DataModel>) msg.obj;
                setAdapterWithData(datalist);
            } else if (msg.what == LoadDataThread.LOADATA_FAIL) {

            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        mRecyclerView = (RecyclerView) findViewById(R.id.lst);
        loader_progressbar = (ProgressBar) findViewById(R.id.loader_progressbar);

        swipeLayout = (SwipeRefreshLayout)
                findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callThreadToLoadData();

            }
        });

        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        if (!isNetworkOnlineTest(getApplicationContext(), swipeLayout)) {
            return;
        }
        loader_progressbar.setVisibility(View.VISIBLE);
        new LoadDataThread(getApplicationContext(), mHandler).start();
    }


    private void setAdapterWithData(ArrayList<DataModel> mydatalist)
    {
        mAdapter = new DataAdapter(MainActivity.this, mydatalist);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
         mRecyclerView.setAdapter(mAdapter);
    }

    private void callThreadToLoadData() {
        if (!isNetworkOnlineTest(getApplicationContext(), swipeLayout)) {
            return;
        }
      //  showProgress(getString(R.string.refreshing));
        new LoadDataThread(getApplicationContext(), mHandler).start();
    }

    public static boolean isNetworkOnlineTest(Context context, View view) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        Snackbar.make(view, context.getString(R.string.lbl_network_unavailable), Snackbar.LENGTH_SHORT).show();

        return false;

    }

    private void showProgress(String title) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            return;
        }
        mProgressDialog = ProgressDialog
                .show(MainActivity.this, null, title, false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

}
