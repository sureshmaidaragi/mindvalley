package com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.threads;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.models.DataModel;
import com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.network.RequestManager;
import com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.network.ResponseManager;

import java.util.ArrayList;

/**
 * Created by sureshmaidaragi on 05/11/16.
 */

public class LoadDataThread extends Thread {

    private final Context context;
    private final Handler mHandler;

    private static final String OPERATION = "http://pastebin.com/raw/wgkJgazE";
    private static final String TAG = "LoadDataThread";

    public static final int LOADATA_SUCCESS = 10;
    public static final int LOADATA_FAIL = 11;

    public LoadDataThread(Context context, Handler mHandler) {
        this.context = context;
        this.mHandler = mHandler;

    }

    @Override
    public void run() {

        String response = RequestManager.sendRequest(OPERATION);

        Log.d(TAG, response);
        if (response == null) {
            sendFailure();
            Log.d(TAG, "response is null");
            return;
        }

        ArrayList<DataModel> datalist = ResponseManager.getData(response);

        if(datalist!=null)
        {
            sendSuccess(datalist);

        }
         else {
            sendFailure();
        }

    }

    private void sendSuccess(ArrayList<DataModel> datalist) {
        Message msg = new Message();
        msg.what = LOADATA_SUCCESS;
        msg.obj = datalist;
        mHandler.sendMessage(msg);
    }

    private void sendFailure() {
        mHandler.sendEmptyMessage(LOADATA_FAIL);
    }

}
