package com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.network;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by sureshmaidaragi on 05/11/16.
 */

public class RequestManager {

    public static String sendRequest(String url) {


        OkHttpClient client = new OkHttpClient();

        // code request code here
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = null;
        String response_body = null;
        try {
            response = client.newCall(request).execute();
            response_body = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        System.out.println("GOT the=" + response.body().toString());
        return response_body;

    }
}
