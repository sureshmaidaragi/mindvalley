package com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.example.load_image_library.LoadImage;
import com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.R;
import com.example.sureshmaidaragi.mindvalley_suresh_maidaragi_android_test.models.DataModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sureshmaidaragi on 05/11/16.
 */

public class DataAdapter  extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private List<DataModel> datalist;
    private Dialog dialog;

    private  Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image_preview;

        public ViewHolder(View view) {
            super(view);
            image_preview = (ImageView) view.findViewById(R.id.image_preview);
         }
    }


    public DataAdapter(Context context, List<DataModel> datalist) {
        this.datalist = datalist;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.data_row_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final DataModel dataModel = datalist.get(position);
        //Pass the image url and imageview to library
        LoadImage loadImage = new LoadImage(context);
        loadImage.displayImage(dataModel.getSmall_url(),holder.image_preview); //Used my own library to load the images
        holder.image_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imgShow(dataModel.getRegular_url(), context);
            }
        });
      }

    @Override
    public int getItemCount() {
        return datalist.size();
    }
    public void imgShow(String message, Context mContext) {
        // //Log("path", message);

        if (dialog != null) {
            dialog.dismiss();
        }
        dialog = new Dialog(mContext, R.style.CustomDialogTheme);
        ImageView Img;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(R.layout.dialog_imagview);
        Img = (ImageView) dialog.findViewById(R.id.img);
        dialog.setCancelable(true);
        // Img.setImageDrawable(Drawable.createFromPath(message));

        Button close_btn = (Button) dialog.findViewById(R.id.btn_close);
        close_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        try {
            //Bitmap bmp = BitmapFactory.decodeFile(message);
            //Img.setImageBitmap(bmp);
           // System.out.println("MAAAAA"+message);
            //LoadImage dialoImageLoade= new LoadImage(context);
           //dialoImageLoade.displayImage(message,Img);
            Picasso.with(context).load(message).into(Img);// ImageLoad didn't help me for url

            dialog.show();
        } catch (Exception e) {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }
}